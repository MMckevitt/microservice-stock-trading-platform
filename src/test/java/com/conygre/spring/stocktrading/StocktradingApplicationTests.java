package com.conygre.spring.stocktrading;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collection;
import java.util.List;

import com.conygre.spring.stocktrading.entities.Stock;
import com.conygre.spring.stocktrading.service.StockService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class StocktradingApplicationTests {


	@Test
	void contextLoads() {
	}

	@Autowired
	private StockService service;
	// private Stock stock;
	// private String testString = "FB";

	@Test
	void addStock(){
		service.addToStockDB(new Stock("Test","Test"));

	}

	@Test
	void getAll() {
		service.addToStockDB(new Stock("Test","test"));
		Collection <Stock> stock = service.getAllStocks();
		assertNotNull(stock, "Test Fail, getAll() failed");
	}

	@Test
	void getStockByTicker(){
		// service.addToStockDB(new Stock("Test", "test"));
		List <Stock> stock = service.getStockByTicker("Test");
		assertNotNull(stock, "Test Fail, getTickerById() failed");
	}

	@Test
	void setGetTickerTest(){
		Stock stock = new Stock();
		stock.setticker("Test");
		assertTrue(stock.getticker() == "Test");
	}

	@Test
	void setGetAmountTest(){
		Stock stock = new Stock();
		stock.setAmount(45);
		assertTrue(stock.getAmount() == 45);
	}

	@Test
	void setGetStateTest(){
		Stock stock = new Stock();
		stock.setState("Test");
		assertTrue(stock.getState() == "Test");
	}

	@Test
	void setGetTypeTest(){
		Stock stock = new Stock();
		stock.setType("Test");
		assertTrue(stock.getType() == "Test");
	}

	@Test
	void setGetPriceTest(){
		Stock stock = new Stock();
		stock.setPrice(11.11);
		assertTrue(stock.getPrice() == 11.11);
	}
	
}
