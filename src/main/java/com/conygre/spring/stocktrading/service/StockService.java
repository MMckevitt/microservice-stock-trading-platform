package com.conygre.spring.stocktrading.service;

import java.util.Collection;
//import java.util.Optional;
import java.util.List;

import com.conygre.spring.stocktrading.data.StockRepository;
//import com.conygre.spring.stocktrading.data.StockRepository;
import com.conygre.spring.stocktrading.entities.Stock;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StockService {

    @Autowired
    private StockRepository dao;

    public void addToStockDB(Stock stock) {
        dao.insert(stock);
    }

    public Collection<Stock> getAllStocks() {
        return dao.findAll();
    }

    public List<Stock> getStockByTicker(String ticker) {
		return dao.findByTicker(ticker);
    }

    public void remove(ObjectId id){
        dao.deleteById(id);

    }

    


}
