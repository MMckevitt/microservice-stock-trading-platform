package com.conygre.spring.stocktrading.dao;

import com.conygre.spring.stocktrading.entities.Stock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;



@Repository
public class StockDAOMongo implements StockDAO{

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void addStock(Stock stock){
        mongoTemplate.insert(stock);
    }

    @Override
    public Stock getStockByTicker(String ticker){
        Query query = new Query();
        query.addCriteria(Criteria.where("ticker").is(ticker));
        Stock stock = mongoTemplate.findOne(query, Stock.class);
        return stock;
    }
    
    @Override
    public Collection<Stock> getAllStocks() {
        return mongoTemplate.findAll(Stock.class);
    }

}
