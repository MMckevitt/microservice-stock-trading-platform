package com.conygre.spring.stocktrading.entities;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Stock {

	@Id
    private ObjectId id;
    private String ticker;
    private String state;
    private int amount;
    private double price;
    private String type;


    public Stock(String ticker, String  state) {
        this.ticker = ticker;
        this.state = state;
    }

    public Stock() {
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getticker() {
        return ticker;
    }

    public void setticker(String ticker) {
        this.ticker = ticker;
    }

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
