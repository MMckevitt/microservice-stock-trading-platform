package com.conygre.spring.stocktrading.rest;

import com.conygre.spring.stocktrading.service.StockService;

import java.util.List;

//import java.util.Optional;

import com.conygre.spring.stocktrading.entities.Stock;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/api/stock")
@CrossOrigin // allows requests from all domains
public class StockController {

    @Autowired
	private StockService service;

	/**
	 * Find All - returnes a collection of all stocks
	 * found within the stockDB database. 
	 * @return - collection
	 */
	@RequestMapping(method = RequestMethod.GET)
	public Iterable<Stock> findAll() {
		return service.getAllStocks();
	}

	/**
	 * Find by Ticker - Finds all records of stocks in the stockDB that 
	 * are of a specified ticker value. e.g. "FB" returns all Facebook
	 * records
	 * @param ticker - stock you want returned 
	 * @return - collection
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/{ticker}")
	public List <Stock> getStockByTicker(@PathVariable("ticker") String ticker) {
		return service.getStockByTicker(ticker);
	}


	/**
	 * addStock - Post method that adds an instance of class stock to the 
	 * stockDB Database
	 * @param stock - instance of Stock.class
	 */
	@RequestMapping(method = RequestMethod.POST)
	public void addStock(@RequestBody Stock stock) {
		service.addToStockDB(stock);
	}

	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public void deleteStockById(@PathVariable("id")String id){
		service.remove(new ObjectId(""+id));
	}


}
